
if &background == 'light'
  hi CursorLine ctermbg=lightgray
  hi ColorColumn ctermbg=lightgray
else
  hi CursorLine ctermbg=darkgray
  hi ColorColumn ctermbg=darkgray
endif

